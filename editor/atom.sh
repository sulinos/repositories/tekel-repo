export NAME="atom"
export MIMETYPE="text/plain;"
export ICON="$(pwd)/atom.png"
export COMMENT="A hackable text editor for the 21st Century."
export CATEGORY="GNOME;GTK;Utility;TextEditor;Development;"
export EXEC="$(pwd)/atom"
_setup(){
  #download archive file
  wget "https://atom.io/download/deb" -O output.deb
}

_build(){
  #extract and move files
  ar x output.deb
  tar -xf data.tar.xz
  rm -f output.deb
  rm -f control.tar.gz
  rm -f data.tar.xz
  rm -f debian-binary
  mv usr/share/atom/* .
  rm -rf usr
}
_install(){
  :
}
