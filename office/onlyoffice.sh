export NAME="Libre Office"
export MIMETYPE=""
export ICON="$(pwd)/soffice.png"
export COMMENT="Open source office application"
export CATEGORY="Office;"
export EXEC="$(pwd)/soffice.appimage"
_setup(){
  #download archive file
  wget -c "https://github.com/ONLYOFFICE/DesktopEditors/releases/download/ONLYOFFICE-DesktopEditors-5.5.1/onlyoffice-desktopeditors-x64.tar.gz" -O ooffice.tar.gz
}

_build(){
  #extract and move files
  tar -xf ooffice.tar.gz
}
_install(){
:
}
