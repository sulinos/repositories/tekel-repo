export NAME="libreoffice"
export MIMETYPE=""
export ICON="$(pwd)/soffice.png"
export COMMENT="Open source office application"
export CATEGORY="Office;"
export EXEC="$(pwd)/program/soffice"
export desktop_files=(base draw math writer calc impress startcenter xsltfilter)
_setup(){
  #download archive file
  [ -f soffice.tar.gz ] || wget -c "https://download.documentfoundation.org/libreoffice/stable/7.0.2/deb/x86_64/LibreOffice_7.0.2_Linux_x86-64_deb.tar.gz" -O soffice.tar.gz
}

unpack_deb(){
	while read name ; do
		echo "Unpacking $name"
		ar x $name
		rm -f control.tar.* debian-binary
		tar -xf data.tar.*
		rm -f data.tar.*
	done
}

_build(){
  #extract and move files
  tar -xf soffice.tar.gz
  cd LibreOffice_*/DEBS
  ls *.deb | unpack_deb
  rm -rf *.deb
  sed -i "s|Exec=libreoffice7.0|Exec=$APPDIR/$NAME/program/soffice|g" opt/*/share/xdg/*.desktop
  sed -i "s|Icon=|Icon=$APPDIR/$NAME/icons/|g" opt/*/share/xdg/*.desktop
  sed -i "s|Icon=.*|&.png|g" opt/*/share/xdg/*.desktop
  mv opt/*/* $APPDIR/$NAME/ || true
  mkdir $APPDIR/$NAME/icons
  mv usr/share/icons/gnome/128x128/apps/* $APPDIR/$NAME/icons/ || true
  rm -rf $APPDIR/$NAME/LibreOffice_*
}
_install(){
  cd $APPDIR/$NAME/
  chmod +x share/xdg/*.desktop
  if [ $UID -eq 0 ] ; then
  	cp -prfv share/xdg/*.desktop /usr/share/applications/
  else
  	cp -prfv share/xdg/*.desktop ~/.local/share/applications/
  fi
}
