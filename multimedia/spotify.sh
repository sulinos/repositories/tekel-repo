export NAME="Spotify"
export MIMETYPE="x-scheme-handler/spotify;"
export ICON="$(pwd)/icons/spotify-linux-48.png"
export COMMENT="Music Player"
export CATEGORY="Audio;Music;Player;AudioVideo;"
export EXEC="$(pwd)/spotify"
_setup(){
  #download archive file
  wget "https://repository-origin.spotify.com/pool/non-free/s/spotify-client/spotify-client_1.1.26.501.gbe11e53b-15_amd64.deb" -O output.deb
}

_build(){
  #extract and move files
  ar x output.deb
  tar -xf data.tar.gz
  rm -f output.deb
  rm -f control.tar.gz
  rm -f data.tar.gz
  rm -f debian-binary
  mv usr/share/spotify/* .
  cp -prfv /usr/lib/libcurl.so.4.6.0 libcurl-gnutls.so.4 #dependency fix
  rm -rf usr
}
_install(){
:
}
