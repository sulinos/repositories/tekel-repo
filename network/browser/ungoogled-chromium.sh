export NAME="Ungoogled-Chromium"
export MIMETYPE="text/html;text/xml;application/xhtml_xml;"
export ICON="$(pwd)/product_logo_48.png"
export COMMENT="Web Browser"
export CATEGORY="Network;Browser;"
export EXEC="$(pwd)/chrome"
_setup(){
  #download archive file
  wget "https://github.com/Eloston/ungoogled-chromium-binaries/releases/download/80.0.3987.106-1.1/ungoogled-chromium_80.0.3987.106-1.1_linux.tar.xz" -O output.tar.xz
}

_build(){
  #extract and move files
  tar -xf output.tar.xz
  rm -f output.tar.xz
  mv ungoogled-chromium_*/* .
  rm -rf ungoogled-chromium_*
  rm -f chrome-wrapper
}
_install(){
:
}
