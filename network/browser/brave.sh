export NAME="Brave"
export MIMETYPE="text/html;text/xml;application/xhtml_xml;"
export ICON="$(pwd)/product_logo_128_nightly.png"
export COMMENT="Web Browser"
export CATEGORY="Network;Browser;"
export EXEC="$(pwd)/brave -- %u"
_setup(){
  #download archive file
  wget -c "https://github.com/brave/brave-browser/releases/download/v1.11.41/brave-browser-nightly-1.11.41-1.x86_64.rpm" -O brave.rpm
}
_build(){
  7z x brave.rpm
  7z x *.cpio
  rm -rf *.cpio
  mv ./opt/brave*/brave*/* ./
  rm -rf etc usr opt
  rm -rf *.cpio
  chmod +x *
}
_install(){
:
}
