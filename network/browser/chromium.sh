export NAME="Chromium"
export MIMETYPE="text/html;text/xml;application/xhtml_xml;"
export ICON="$(pwd)/product_logo_48.png"
export COMMENT="Web Browser"
export CATEGORY="Network;Browser;"
export EXEC="$(pwd)/chrome"
_setup(){
  #download archive file
  wget "https://download-chromium.appspot.com/dl/Linux_x64?type=snapshots" -O output.zip
}

_build(){
  #extract and move files
  unzip output.zip
  rm -f output.zip
  mv chrome-linux/* .
  rm -rf chrome-linux
  rm -f chrome-wrapper
}
_install(){
:
}
