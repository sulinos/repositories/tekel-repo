export NAME="Firefox"
export MIMETYPE="text/html;text/xml;application/xhtml_xml;"
export ICON="$(pwd)/firefox/browser/chrome/icons/default/default64.png"
export COMMENT="Web Browser"
export CATEGORY="Network;Browser;"
export EXEC="$(pwd)/firefox/firefox-bin -- %u"
_setup(){
  #download archive file
  wget "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64" -O firefox.tar.bz2
}
_build(){
  tar -xf firefox.tar.bz2
  rm -rf firefox.tar.bz2
}
_install(){
:
}
