export NAME="telegramdesktop"
export MIMETYPE=""
export ICON="telegram"
export COMMENT="Official desktop version of Telegram messaging app"
export CATEGORY="Chat;Network;InstantMessaging;Qt;"
export EXEC="$(pwd)/Telegram"
_setup(){
  wget "https://telegram.org/dl/desktop/linux" -O telegram.tar.gz
}

_build(){
  tar -xf telegram.tar.gz
  rm -f telegram.tar.gz
  mv Telegram tg
  mv tg/* .
  rm -rf tg
}
_install(){
:
}
