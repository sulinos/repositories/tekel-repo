export NAME="Discord"
export MIMETYPE=""
export ICON="$(pwd)/discord.png"
export COMMENT="All-in-one voice and text chat for gamers that's free, secure, and works on both your desktop and phone."
export CATEGORY="Network;InstantMessaging;"
export EXEC="$(pwd)/Discord"
_setup(){
  wget "https://discord.com/api/download?platform=linux&format=tar.gz" -O discord.tar.xz
}
_build(){
  tar -xf discord.tar.xz
  rm -rf discord.tar.xz
  mv Discord dc
  mv dc/* .
  rm -rf dc
}
_install(){
    chmod +x Discord
}
