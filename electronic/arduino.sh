export NAME="Arduino"
export MIMETYPE="text/plain;"
export ICON="$(pwd)/lib/arduino.png"
export COMMENT="Arduino IDE."
export CATEGORY="Development;"
export EXEC="$(pwd)/arduino"
_setup(){
  #download archive file
  wget "https://downloads.arduino.cc/arduino-1.8.12-linux64.tar.xz" -O output.tar.xz
}

_build(){
  #extract and move files
  tar -xf output.tar.xz
  mv arduino-1.8.12/* .
  rm -rf arduino-1.8.12
  rm -f output.tar.xz
}
_install(){
:
}
